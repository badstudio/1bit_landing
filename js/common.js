$(document).ready(function() {
    isMobile = {Android: function() {return navigator.userAgent.match(/Android/i);}, BlackBerry: function() {return navigator.userAgent.match(/BlackBerry/i);}, iOS: function() {return navigator.userAgent.match(/iPhone|iPad|iPod/i);}, Opera: function() {return navigator.userAgent.match(/Opera Mini/i);}, Windows: function() {return navigator.userAgent.match(/IEMobile/i);}, any: function() {return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());}};
    Overlay();
    FormAjaxPost("body");
    phonemask("body");
    BSTooltip();
    Carousel();
    Fancybox("body");
});

//default
function phonemask(parent){$("[name='phone'], [type='tel'], .js-mask-phone", parent).mask("+7 (999) 999-99-99");}
function validateEmail(a){let t=/^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;return t.test(a)}
function sendPhoneEvent() {
    if (!isMobile.any()) {
        $(".js-callback-open-trigger").trigger("click");
        event.preventDefault();
    }
}
//Overlay Functions
function Overlay() {
    $(document).keyup(function (e) {
        if ($(".js-overlay-content").html().length) {
            if (e.keyCode === 27) OverlayClose();
        }
    });
    $(document).on("click", ".overlay", function (e) {
        if ($(e.target).closest($(".overlay-window", ".overlay")).length) return;
        OverlayClose();
        e.stopPropagation();
    });
    let body = $("body"),
        overlay = $(".overlay"),
        overlayWindow = $(".overlay-window");
    $(".js-OverlayOpen").on("click", function(e) {
        e.preventDefault();
        let t = $(this),
            href = (t.attr("href")) ? t.attr("href") : "",
            title = (t.attr("title")) ? t.attr("title") : "",
            id = (t.attr("data-id")) ? t.attr("data-id") : "";
        title = (t.attr("data-title")) ? t.attr("data-title") : title;
        href = (t.attr("data-href")) ? t.attr("data-href") : href;
        $.ajax({
            type: 'post',
            url: href,
            data: "&ajax=Y&title=" + title+"&id="+id,
            success: function (data) {
                $(".js-overlay-title").html(title);
                $(".js-overlay-content").html(data);
                setTimeout(function() {
                    body.css({"overflow": "hidden", "padding-right": "15px"});
                    overlay.fadeIn(300, function() {
                        let overlayWindowHeight = overlayWindow.outerHeight(),
                            overlayWindowWidth = overlayWindow.outerWidth(),
                            mTop = (overlayWindowHeight/2)*(-1),
                            top = "50%";
                        if (overlayWindowHeight > $(window).outerHeight()) {
                            mTop = 20;
                            top = 0;
                            overlay.css({"overflow-y": "auto"});
                        }
                        overlayWindow.css({
                            "margin-left": (overlayWindowWidth/2)*(-1),
                            "margin-top": mTop,
                            "top": top,
                            "left": "50%",
                        });
                    });
                    FormAjaxPost(overlay);
                    phonemask(overlay);
                }, 300);
            }
        });
    });
    $(".js-OverlayClose").on("click", function() {
        OverlayClose();
    });
}

function OverlayClose() {
    let body = $("body"),
        overlay = $(".overlay"),
        overlayWindow = $(".overlay-window");
    overlayWindow.css({
        "top": "-100%"
    });
    overlay.fadeOut(300, function() {
        $(".js-overlay-title").html('');
        $(".js-overlay-content").html('');
    });
    body.css({"overflow": "unset", "padding-right": "unset"});
}
//end Overlay Functions

//Form Functions
function FormAjaxPost(parent) {
    if ($(".dropzone-block", parent).length) {
        $(".dropzone-block", parent).on("click", ".js-dropzone-remove", function() {
            let t = $(this),
                parent = t.parents(".dropzone-preview");
            parent.fadeOut(300, function() {
                $(this).remove();
            })
        });
    }
    let form = $("form.form--ajax-post", parent);
    form.submit(function (e) {
        e.preventDefault();
        let t = $(this),
            submit = $("button", t),
            serialize = t.serialize(),
            method = t.attr("method"),
            url = t.attr("action"),
            formError = 0,
            textError = "",
            formStatus = $(".result-message", t),
            yandexCeil = ($("[name='yandexCeil']").val()) ? $("[name='yandexCeil']").val() : '';
        submit.fadeOut(1);
        formStatus.html('');
        $("input.required, textarea.required", t).each(function () {
            if ($(this).attr("type") != 'checkbox') {
                if (!$(this).val().length) {
                    $(this).addClass("validate").keypress(function () {
                        $(this).removeClass("validate");
                    });
                    let label = $(this).attr("data-help");
                    textError += "Поле \"" + label + "\" обязательно для заполнения.<BR>";
                    formError = 1;
                }
            }
        });
        if ($("[name='email'], .js-email-required", t).length) {
            $("[name='email'], .js-email-required", t).each(function () {
                if ($(this).val().length) {
                    if (!validateEmail($(this).val())) {
                        $(this).addClass("validate").keypress(function () {
                            $(this).removeClass("validate");
                        });
                        let label = $(this).attr("data-help");
                        textError += "Поле \"" + label + "\" заполнено не верно.<BR>";
                        formError = 1;
                    }
                }
            });
        }
        if ($("select.required", t).length) {
            $("select.required", t).each(function() {
                let t = $(this),
                    label = t.attr("data-help");
                if ($("option:selected", t).val().length == 0) {
                    $(this).addClass("validate").change(function () {
                        $(this).removeClass("validate");
                    });
                    textError += "Поле \"" + label + "\" заполнено не верно.<BR>";
                    formError = 1;
                }
            });
        }
        if ($(".js-min-length", t).length) {
            $(".js-min-length", t).each(function() {
                let minLength = parseInt($(this).attr("data-length"));
                if ($(this).val().length) {
                    if ($(this).val().length < minLength) {
                        $(this).addClass("validate").keypress(function () {
                            $(this).removeClass("validate");
                        });
                        let label = $(this).attr("data-help");
                        textError += "Количество символов в поле \"" + label + "\" должно быть не менее: "+minLength+" <BR>";
                        formError = 1;
                    }
                }
            });
        }
        $("[type='checkbox'].required", t).each(function() {
            let t = $(this),
                text = t.attr("data-help");
            if (t.prop("checked") == false) {
                textError += text+"<BR>";
                formError = 1;
            }
        });
        if (formError == 1) {
            formStatus.addClass("formError").html(textError);
            submit.fadeIn(1);
        } else {
            $.ajax({
                type: method,
                typedata: 'json',
                url: url,
                data: serialize + "&ajax=Y&link=" + window.location.href,
                success: function (data) {
                    let jsonString = data,
                        result = JSON.parse(jsonString);
                    if (result.success == true) {
                        submit.remove();
                        formStatus.addClass("formSuccess").html(result.message);
                        /*
                        setTimeout(function () {
                            CloseOverlay();
                        }, 7000);
                        */
                    } else {
                        submit.fadeIn(1);
                        formStatus.addClass("formError").html(result.message);
                    }
                    if (result.reload) {
                        if (result.reload !== false) {
                            window.location.href = result.reload;
                        }
                    }
                }
            });
        }
    });
}

function BSTooltip() {
    let el = $('[data-toggle="tooltip"]');
    if (el.length) {
        $('[data-toggle="tooltip"]').tooltip();
    }
}

function Carousel() {
    let el = $(".component_carousel");
    if (el.length) {
        let owl = $(".owl-carousel", el);
        owl.owlCarousel({
            items: 1,
            dots: true,
            nav: false,
            autoHeight: true
        })
    }
}

//FANCYBOX
function Fancybox(parent) {
    $(".js-fancybox", parent).fancybox();
    $(".js-fancybox-iframe", parent).fancybox({
        infobar : false,
        buttons : ['close'],
        type: 'iframe',
        btnTpl : {
            close : '<div data-fancybox-close title="Закрыть окно">' +
                '<span class="icon-close"></span>' +
                '</div>',
        }
    });
}