<?
$protocol = 'http';
if ((isset($_SERVER['REQUEST_SCHEME']) AND $_SERVER['REQUEST_SCHEME'] === 'https') OR (isset($_SERVER['HTTPS']) AND $_SERVER['HTTPS'] === 'on')) {
    $protocol = 'https';
}
$HUMANS = $protocol . "://" . $_SERVER['HTTP_HOST'] . "/humans.txt";
$HOST = $_SERVER['HTTP_HOST'];
$WWW = $protocol . "://" . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
?>
<!DOCTYPE html>
<html lang="ru">
<head itemscope itemtype="http://schema.org/WebSite">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="SKYPE_TOOLBAR" content="SKYPE_TOOLBAR_PARSER_COMPATIBLE">
    <meta name="format-detection" content="telephone=no">
    <title itemprop="name">Аудит бизнеса в Краснодаре</title>
    <link rel="apple-touch-icon" sizes="180x180" href="/favicon/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/favicon/favicon-16x16.png">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="theme-color" content="#ffffff">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="mobile-web-app-capable" content="yes">
    <meta name="mobile-web-app-status-bar-style" content="black">
    <meta property="og:type" content="website">
    <meta property="og:title" content="Аудит Бизнеса в Краснодаре">
    <meta property="og:site_name" content="Аудит Бизнеса">
    <meta property="og:url" content="<?= $WWW; ?>">
    <link itemprop="url" href="<?= $WWW; ?>">
    <link rel="home" href="<?= $protocol . "://" . $_SERVER['HTTP_HOST']; ?>">
    <link href="/css/styles.css" rel="stylesheet">
    <script src="/js/jquery-3.3.1.min.js"></script>
</head>

<body>
<div class="overlay"><div class="overlay-window position-relative"><span class="overlay-close icons-close js-OverlayClose"></span><div class="overlay-header d-flex"><div class="overlay-title js-overlay-title"></div></div><div class="overlay-content js-overlay-content"></div></div></div>
<div class="js-callback-open-trigger d-none js-OverlayOpen" data-href="/forms/callback.php" title="Заказать звонок"></div>
<div class="wrapper d-flex flex-column">
    <? include_once("header.php"); ?>
    <div class="content-block">
        <main>
            <div class="page-block_1 pt-3 pb-3 d-flex align-items-center">
                <div class="container">
                    <div class="row">
                        <div class="col-12 col-lg-7 h-100">
                            <div class="p-bg block_1-title d-none d-md-block">
                                <p>Аудит бизнеса – верный способ</p>
                                <p>сэкономить 300 000 рублей</p>
                            </div>
                            <div class="p-bg block_1-title d-block d-md-none">
                                <p>Аудит бизнеса – верный способ сэкономить 300 000 рублей</p>
                            </div>
                        </div>
                        <div class="w-100 mt-md-5"></div>
                        <div class="col-12 col-lg-6 mt-5">
                            <div class="d-flex align-items-center mb-4 white-bg">
                                <span class="icons-arrow-right-drop mr-3 pink-color"></span>
                                <div>
                                    Позволит принять обоснованное решение о необходимости автоматизации бизнеса
                                </div>
                            </div>

                            <div class="d-flex align-items-center mb-4 white-bg">
                                <span class="icons-arrow-right-drop mr-3 pink-color"></span>
                                <div>
                                    Укажет на пробелы в «отлаженных» бизнес-процессах, точки потерь, этапность
                                    автоматизации
                                </div>
                            </div>

                            <div class="d-flex align-items-center mb-4 white-bg">
                                <span class="icons-arrow-right-drop mr-3 pink-color"></span>
                                <div>
                                    Определит программные продукты, подходящие для автоматизации существующих
                                    бизнес-процессов
                                </div>
                            </div>
                        </div>
                        <div class="w-100"></div>
                        <div class="button js-OverlayOpen d-block text-center ml-5 mt-2 mt-md-5"
                             data-href="/forms/callback.php" title="Получить консультацию специалиста">Расскажем
                            подробнее
                        </div>
                    </div>
                </div>
            </div>

            <div class="page-block_2 pt-3 pb-3 pt-md-5 pb-md-5">
                <div class="container">
                    <div class="row mt-3 mt-md-5">
                        <div class="col-12">
                            <div class="block-title d-table m-auto mb-3 mb-md-5">Когда нужен аудит бизнеса?</div>
                        </div>
                    </div>
                    <div class="component_steps_audit mt-3 mt-md-5">
                        <div class="row">
                            <div class="col-12 col-md-6 col-lg-4 d-flex mb-3 mt-3 mb-md-5 mt-md-5">
                                <div class="step_number mr-4">01</div>
                                <div class="step_detail">
                                    Данные оперативного и управленческого учета существуют в разных системах. Требуется
                                    создать единую информационную систему
                                </div>
                            </div>
                            <div class="col-12 col-md-6 col-lg-4 d-flex mb-3 mt-3 mb-md-5 mt-md-5">
                                <div class="step_number mr-4">02</div>
                                <div class="step_detail">
                                    Требуется правильно выстроить и формализовать процессы
                                </div>
                            </div>
                            <div class="col-12 col-md-6 col-lg-4 d-flex mb-3 mt-3 mb-md-5 mt-md-5">
                                <div class="step_number mr-4">03</div>
                                <div class="step_detail">
                                    в автоматизацию вложено много средств, но система «не взлетает»
                                </div>
                            </div>
                            <div class="col-12 col-md-6 col-lg-4 d-flex mb-3 mt-3 mb-md-5 mt-md-5">
                                <div class="step_number mr-4">04</div>
                                <div class="step_detail">
                                    Требуется принять решение по проекту комплексной автоматизации всех ключевых
                                    бизнес-процессов
                                </div>
                            </div>
                            <div class="col-12 col-md-6 col-lg-4 d-flex mb-3 mt-3 mb-md-5 mt-md-5">
                                <div class="step_number mr-4">05</div>
                                <div class="step_detail">
                                    Необходимо минимизировать будущие затраты на эксплуатацию и доработку информационной
                                    системы
                                </div>
                            </div>
                            <div class="col-12 col-md-6 col-lg-4 d-flex mb-3 mt-3 mb-md-5 mt-md-5">
                                <div class="step_number mr-4">06</div>
                                <div class="step_detail">
                                    Принято решение о постановке бюджетирования в компании
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="page-block_3 pt-3 pb-3 pt-md-5 pb-md-5 d-lg-flex align-items-lg-center">
                <div class="container">
                    <div class="row mt-3 mt-md-5">
                        <div class="col-12">
                            <div class="block-title d-table m-auto mb-3 mb-md-5 white-color">Аудит включает в себя:</div>
                        </div>
                    </div>
                    <div class="row component_audit_in mt-5 mb-5 pt-5">
                        <div class="col-12 col-md-6 col-lg-3 text-center mb-5">
                            <div class="audit_in-icon mb-4">
                                <span class="icons-in_interview"></span>
                                <span class="square"></span>
                            </div>
                            <div class="audit_in-title mt-4">Интервью с ключевыми сотрудниками</div>
                        </div>
                        <div class="col-12 col-md-6 col-lg-3 text-center mb-5">
                            <div class="audit_in-icon mb-4">
                                <span class="icons-in_map"></span>
                                <span class="square"></span>
                            </div>
                            <div class="audit_in-title mt-4">Составление карты бизнес - процессов "как есть"</div>
                        </div>
                        <div class="col-12 col-md-6 col-lg-3 text-center mb-5">
                            <div class="audit_in-icon mb-4">
                                <span class="icons-in_diagnostic"></span>
                                <span class="square"></span>
                            </div>
                            <div class="audit_in-title mt-4">Диагностику текущих бизнес - процессов и ИТ систем</div>
                        </div>
                        <div class="col-12 col-md-6 col-lg-3 text-center mb-5">
                            <div class="audit_in-icon mb-4">
                                <span class="icons-in_map_final"></span>
                                <span class="square"></span>
                            </div>
                            <div class="audit_in-title mt-4">Составление карты бизнес - процессов “как должно быть”</div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12 text-center">
                            <div class="button js-OverlayOpen d-inline-block" data-href="/forms/callback.php" title="Заказать аудит компании">Заказать аудит компании</div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="page-block_4 pt-3 pb-3 pt-md-5 pb-md-5">
                <div class="container">
                    <div class="row mt-3 mt-md-5">
                        <div class="col-12">
                            <div class="block-title d-table m-auto mb-3 mb-md-5">Что в результате?</div>
                        </div>
                    </div>
                    <div class="row pb-5">
                        <div class="col-12 col-md-8 col-lg-6 fs16 mb-4 mb-md-0">
                            По итогам проведенного аудита Вы получите схему бизнес-процессов по <span
                                    data-toggle="tooltip" data-placement="bottom" data-html="true" title="<span class='tooltip-title'>EPC (событийная цепочка процессов)</span>
<p>тип блок-схемы для бизнес-моделирования. Используется для описания процессов нижнего уровня. Диаграмма процесса в нотации EPC, представляет собой упорядоченную комбинацию событий и функций. Для каждой функции могут быть определены начальные и конечные события, участники, исполнители, материальные и документальные потоки, сопровождающие её, а также проведена декомпозиция на более низкие уровни.</p>">EPC</span>*
                            «как есть» и «как
                            должно быть». А также рекомендуемый список программного обеспечения с этапами, сроками,
                            стоимостью и приоритетами по автоматизации.
                        </div>

                        <div class="col-12 col-md-4 ml-md-auto block-attention">
                            ! Мы можем рекомендовать отказ от автоматизации, так как уверены, что автоматизировать хаос
                            невозможно
                        </div>
                    </div>

                    <div class="component_audit_result row pt-md-5">
                        <div class="col-12 col-md-3 ml-md-auto mr-md-auto mb-4 mb-md-0">
                            <div class="audit_result-item d-flex">
                                <div class="audit_result-count">01</div>
                                <div class="audit_result-name">
                                    <p>Отчет по результатам</p>
                                    <p>аудита</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-md-3 ml-md-auto mr-md-auto mb-4 mb-md-0">
                            <div class="audit_result-item d-flex">
                                <div class="audit_result-count">02</div>
                                <div class="audit_result-name">
                                    <p>Защита итогов аудита</p>
                                    <p>перед руководством</p>
                                    <p>компании</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-md-3 ml-md-auto mr-md-auto mb-4 mb-md-0">
                            <div class="audit_result-item d-flex">
                                <div class="audit_result-count">03</div>
                                <div class="audit_result-name">
                                    <p>Проектное</p>
                                    <p>предложение</p>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row pt-4">
                        <div class="col-12 col-md-6 ml-md-auto mr-md-auto order-2 order-md-1 mt-4 mt-md-0">
                            <div class="component_carousel">
                                <div class="owl-carousel">
                                    <div><a href="demo/otchet_1.png" data-fancybox="product-gallery"
                                            title="Увеличить изображение {Название товара}"><img
                                                    src="demo/otchet_1.png"></a></div>
                                    <div><a href="demo/otchet_2.png" data-fancybox="product-gallery"
                                            title="Увеличить изображение {Название товара}"><img
                                                    src="demo/otchet_2.png"></a></div>
                                    <div><a href="demo/otchet_3.png" data-fancybox="product-gallery"
                                            title="Увеличить изображение {Название товара}"><img
                                                    src="demo/otchet_3.png"></a></div>
                                    <div><a href="demo/otchet_4.png" data-fancybox="product-gallery"
                                            title="Увеличить изображение {Название товара}"><img
                                                    src="demo/otchet_4.png"></a></div>
                                    <div><a href="demo/otchet_5.png" data-fancybox="product-gallery"
                                            title="Увеличить изображение {Название товара}"><img
                                                    src="demo/otchet_5.png"></a></div>
                                </div>
                            </div>
                        </div>

                        <div class="col-12 col-md-3 order-1 order-md-2 ml-md-auto mr-md-auto">
                            <div class="d-flex mb-4">
                                <span class="icons-arrow-right-drop pink-color mr-3"></span>
                                <div>Предпосылки и ограничения</div>
                            </div>
                            <div class="d-flex mb-4">
                                <span class="icons-arrow-right-drop pink-color mr-3"></span>
                                <div>Предлагаемые рамки проекта</div>
                            </div>
                            <div class="d-flex mb-4">
                                <span class="icons-arrow-right-drop pink-color mr-3"></span>
                                <div>Функциональный объем по очередям</div>
                            </div>
                            <div class="d-flex mb-4">
                                <span class="icons-arrow-right-drop pink-color mr-3"></span>
                                <div>Стоимость и срок выполнения работ</div>
                            </div>
                            <div class="d-flex mb-4">
                                <span class="icons-arrow-right-drop pink-color mr-3"></span>
                                <div>Календарный график</div>
                            </div>
                        </div>

                        <div class="col-12 text-center order-1 order-md-2 mt-4">
                            <div class="button js-OverlayOpen d-inline-block" data-href="/forms/callback.php"
                                 title="Заказать консультацию">Заказать консультацию
                            </div>
                        </div>
                    </div>

                </div>
            </div>

            <div class="page-block_5 pt-3 pb-3 pt-md-5 pb-md-5"></div>

            <div class="page-block_6 pt-3 pb-3 pt-md-5 pb-md-5">
                <div class="container">
                    <div class="row mt-3 mt-md-5">
                        <div class="col-12">
                            <div class="block-title line-small d-table m-auto mb-3 mb-md-5">Наши клиенты</div>
                        </div>
                    </div>
                    <div class="row component_clients d-flex align-items-center justify-content-center">
                        <div class="col-6 col-md-4 col-lg-3 mb-4">
                            <img src="demo/client1.jpg" alt="Производственная компания Прок"
                                 title="Производственная компания Прок">
                        </div>
                        <div class="col-6 col-md-4 col-lg-3 mb-4">
                            <img src="demo/client2.png" alt="Завод современных фасадных решений Ecodeco"
                                 title="Завод современных фасадных решений Ecodeco">
                        </div>
                        <div class="col-6 col-md-4 col-lg-3 mb-4">
                            <img src="demo/client3.png" alt="Аванта" title="Аванта">
                        </div>
                        <div class="col-6 col-md-4 col-lg-3 mb-4">
                            <img src="demo/client4.jpg" alt="Юнона" title="Юнона">
                        </div>
                        <div class="col-6 col-md-4 col-lg-3 mb-4">
                            <img src="demo/client5.jpg" alt="VEGA" title="VEGA">
                        </div>
                        <div class="col-6 col-md-4 col-lg-3 mb-4">
                            <img src="demo/client6.jpg" alt="Южная корона" title="Южная корона">
                        </div>
                    </div>
                </div>
            </div>

            <div class="page-block_7 pt-3 pb-3 pt-md-5 pb-md-5">
                <div class="container">
                    <div class="row mt-3 mt-md-5">
                        <div class="col-12">
                            <div class="block-title line-small d-table m-auto mb-3 mb-md-5">Отзывы</div>
                        </div>
                    </div>
                    <div class="component_comments">
                        <div class="row">
                            <div class="col-12 col-md-6 pt-4 mb-4 d-flex align-items-start pl-0 pr-0">
                                <div class="comment_item pl-4 pr-4 pt-4 d-flex row ml-4 mr-4">
                                    <span class="icons-quote"></span>
                                    <div class="col-auto ml-auto mr-auto">
                                        <div class="comment_ava mb-4 mb-md-0">
                                            <img src="demo/ava.jpg">
                                        </div>
                                    </div>
                                    <div class="col-12 col-lg">
                                        <div class="comment_detail">
                                            <p>Равным образом постоянный количественный рост и сфера нашей активности
                                                влечет за
                                                собой процесс внедрения и модернизации систем массового участия.
                                                Разнообразный и
                                                богатый опыт рамки и место обучения кадров представляет собой интересный
                                                эксперимент
                                                проверки позиций.</p>
                                            <p>Равным образом постоянный количественный рост и сфера нашей активности
                                                влечет за
                                                собой процесс внедрения и модернизации систем массового участия.
                                                Разнообразный и
                                                богатый опыт рамки и место обучения кадров представляет собой интересный
                                                эксперимент
                                                проверки позиций.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-md-6 pt-4 mb-4 d-flex align-items-start pl-0 pr-0">
                                <div class="comment_item pl-4 pr-4 pt-4 d-flex row ml-4 mr-4">
                                    <span class="icons-quote"></span>
                                    <div class="col-auto ml-auto mr-auto">
                                        <div class="comment_ava mb-4 mb-md-0">
                                            <img src="demo/ava.jpg">
                                        </div>
                                    </div>
                                    <div class="col-12 col-lg">
                                        <div class="comment_detail">
                                            <p>Равным образом постоянный количественный рост и сфера нашей активности
                                                влечет за
                                                собой процесс внедрения и модернизации систем массового участия.
                                                Разнообразный и
                                                богатый опыт рамки и место обучения кадров представляет собой интересный
                                                эксперимент
                                                проверки позиций.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-md-6 pt-4 mb-4 d-flex align-items-start pl-0 pr-0">
                                <div class="comment_item pl-4 pr-4 pt-4 d-flex row ml-4 mr-4">
                                    <span class="icons-quote"></span>
                                    <div class="col-auto ml-auto mr-auto">
                                        <div class="comment_ava mb-4 mb-md-0">
                                            <img src="demo/ava.jpg">
                                        </div>
                                    </div>
                                    <div class="col-12 col-lg">
                                        <div class="comment_detail">
                                            <p>Равным образом постоянный количественный рост и сфера нашей активности
                                                влечет за
                                                собой процесс внедрения и модернизации систем массового участия.
                                                Разнообразный и
                                                богатый опыт рамки и место обучения кадров представляет собой интересный
                                                эксперимент
                                                проверки позиций.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-md-6 pt-4 mb-4 d-flex align-items-start pl-0 pr-0">
                                <div class="comment_item pl-4 pr-4 pt-4 d-flex row ml-4 mr-4">
                                    <span class="icons-quote"></span>
                                    <div class="col-auto ml-auto mr-auto">
                                        <div class="comment_ava mb-4 mb-md-0">
                                            <img src="demo/ava.jpg">
                                        </div>
                                    </div>
                                    <div class="col-12 col-lg">
                                        <div class="comment_detail">
                                            <p>Равным образом постоянный количественный рост и сфера нашей активности
                                                влечет за
                                                собой процесс внедрения и модернизации систем массового участия.
                                                Разнообразный и
                                                богатый опыт рамки и место обучения кадров представляет собой интересный
                                                эксперимент
                                                проверки позиций.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="page-block_8 pt-3 pb-3 pt-md-5 pb-md-5 d-flex align-items-center">
                <a class="block_8-phone d-flex justify-content-center align-items-center" href="tel:+78612031312"
                   onclick="sendPhoneEvent()">
                    <span class="icons-phone white-color"></span>
                </a>
                <div class="container">
                    <div class="row">
                        <div class="col-12 col-md-6 col-lg-5">
                            <form class="form--ajax-post" action="/feedback.php" method="post"
                                  enctype="multipart/form-data">
                                <div class="form-title">Рассчитать<br>стоимость аудита</div>

                                <div class="field">
                                    <label for="NAME">Имя:</label>
                                    <input type="text" name="NAME" id="NAME" value="" placeholder="Ваше имя"
                                           data-help="Имя">
                                </div>

                                <div class="field">
                                    <label for="PHONE">Телефон: <span class="red-color">*</span></label>
                                    <input class="required" type="tel" name="PHONE" id="PHONE" value=""
                                           placeholder="Контактный телефон" data-help="Контактный телефон">
                                </div>
                                <div class="field-checkbox">
                                    <input class="required" type="checkbox" id="agreement-1"
                                           data-help="Необходимо ваше согласие на обработку персональных данных"
                                           checked>
                                    <label for="agreement-1">Я даю свое согласие на обработку персональных данных и
                                        соглашаюсь с
                                        <a href="/forms/agreement.php" class="js-OverlayOpen"
                                           title="Политика конфиденциальности">условиями политики
                                            конфиденциальности.</a>
                                    </label>
                                </div>
                                <div class="result-message"></div>
                                <input type="hidden" name="successText" value="Ваша заявка успешно отправлена">
                                <input type="hidden" name="subject" value="Расчет стоимости аудита">
                                <input type="hidden" name="act" value="feedback">
                                <button class="d-inline-block mt-4">Отправить</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

            <div class="page-block_9 pt-3 pb-3 pt-md-5 pb-md-5">
                <div class="container">
                    <div class="row mt-3 mt-md-5">
                        <div class="col-12">
                            <div class="block-title d-table m-auto mb-3 mb-md-5 white-color text-right text-md-center">
                                Хотите сами провести аудит бизнеса?
                            </div>
                        </div>
                    </div>
                    <div class="row mt-3 mt-md-0">
                        <div class="col-11 col-md-7 d-flex ml-auto mr-auto mb-4">
                            <span class="icons-arrow-right-drop pink-color mr-3"></span>
                            <div>Начните с составления карты бизнес-процессов в нотации <span data-toggle="tooltip"
                                                                                              data-placement="bottom"
                                                                                              data-html="true" title="<span class='tooltip-title'>EPC (событийная цепочка процессов)</span>
<p>тип блок-схемы для бизнес-моделирования. Используется для описания процессов нижнего уровня. Диаграмма процесса в нотации EPC, представляет собой упорядоченную комбинацию событий и функций. Для каждой функции могут быть определены начальные и конечные события, участники, исполнители, материальные и документальные потоки, сопровождающие её, а также проведена декомпозиция на более низкие уровни.</p>">EPC</span>*.
                                Это позволит Вам
                                оптимизировать существующие процессы, сократив при этом финансовые потери даже без
                                привлечения сторонних консультантов
                            </div>
                        </div>

                        <div class="col-11 col-md-7 d-flex ml-auto mr-auto mb-4">
                            <span class="icons-arrow-right-drop pink-color mr-3"></span>
                            <div>Создав карту процессов по бесплатному шаблону Вы сможете обратиться к любому
                                интегратору IT решений на рынке для просчета стоимости автоматизации.
                            </div>
                        </div>

                        <div class="col-11 col-md-7 d-flex ml-auto mr-auto mb-4">
                            <span class="icons-arrow-right-drop pink-color mr-3"></span>
                            <div>Принимайте решение об автоматизации грамотно!</div>
                        </div>
                    </div>

                    <div class="row mt-5">
                        <div class="col-12 text-center">
                            <div class="d-flex justify-content-center align-items-center">
                                <a href="javascript: void(0);" class="button d-inline-flex align-items-center">
                                    <span class="icons-file-pdf fs26 mr-4"></span>
                                    <span>Скачать шаблон</span>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </main>
    </div>
</div>
<? include_once("metrika.php"); ?>
<script src="/js/bootstrap.bundle.min.js"></script>
<script src="/js/owl.carousel.min.js"></script>
<script src="/js/jquery.fancybox.min.js"></script>
<script src="/js/jquery.maskedinput.min.js"></script>
<script src="/js/common.js"></script>
</body>
</html>