<header class="pt-3 pb-3 pt-md-5 pb-md-5">
    <div class="js-header-fixed header-fixed">
        <div class="container h-100">
            <div class="row d-flex align-items-center h-100">
                <div class="col-auto mr-auto col-md-3 ml-md-0 mr-md-0">
                    <a href="javascript: void(0);" title="ПервыйБит Аудит Бизнеса"><img src="images/logo.svg"></a>
                </div>
                <div class="ml-auto col-auto d-flex align-items-center">
                    <a class="d-flex align-items-center header-phone" href="tel:+78612031312" onclick="sendPhoneEvent()">
                        <span class="icons-phone pink-color mr-3"></span><span class="d-none d-md-block">+7 861 203-13-12</span>
                    </a>
                </div>
                <div class="col-12 col-md-auto d-none d-md-block">
                    <div class="button js-OverlayOpen d-inline-block" data-href="/forms/callback.php" title="Получить консультацию специалиста">Получить консультацию</div>
                </div>
            </div>
        </div>
    </div>
</header>