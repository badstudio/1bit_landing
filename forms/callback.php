<form class="form--ajax-post ml-auto mr-auto" method="post"
      action="/feedback.php" enctype="multipart/form-data">
    <div class="field">
        <label for="NAME">ФИО:</label>
        <input class="" type="text" name="NAME" id="NAME" placeholder="Ваше имя" data-help="Ваше имя">
    </div>
    <div class="field">
        <label for="NAME">Контактный телефон <span class="red-color">*</span>:</label>
        <input class="required js-mask-phone" type="tel" name="PHONE" id="PHONE" placeholder="Контактный телефон" data-help="Контактный телефон">
    </div>
    <div class="field">
        <label for="EMAIL">E-mail:</label>
        <input class="" type="email" name="EMAIL" id="EMAIL" placeholder="Контактный e-mail" data-help="E-mail">
    </div>
    <input type="hidden" name="successText" value="Ваша заявка успешно отправлена">
    <input type="hidden" name="subject" value="<?= $_POST['title'];?>">
    <input type="hidden" name="act" value="feedback">
    <div class="field">
        <div class="field-checkbox">
            <input class="required" type="checkbox" id="agreement-2" data-help="Необходимо ваше согласие на обработку персональных данных" checked>
            <label for="agreement-2">Я даю свое согласие на обработку персональных данных и соглашаюсь с
                <a href="/forms/agreement.php" class="js-OverlayOpen" title="Политика конфиденциальности" target="_blank">условиями политики конфиденциальности.</a>
            </label>
        </div>
    </div>
    <div class="result-message mt-3 mb-3"></div>
    <button class="w-100">Отправить</button>
</form>